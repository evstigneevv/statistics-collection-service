package ru.evstigneev.scs.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ru.evstigneev.scs.entity.Event;
import ru.evstigneev.scs.enumeration.Classificator;
import ru.evstigneev.scs.enumeration.ExceptionMessage;
import ru.evstigneev.scs.repository.EventRepository;

@Service
public class EventService {

    private final EventRepository eventRepository;

    @Autowired
    public EventService(EventRepository eventRepository) {
        this.eventRepository = eventRepository;
    }

    public Event create(final Event event) {
        if (eventRepository.findById(event.getId()).isPresent()) {
            throw new IllegalArgumentException(ExceptionMessage.ENTITY_ALREADY_EXISTS.toString());
        }
        return eventRepository.save(event);
    }

    public Page<Event> findAllOrderByDateDesc(Pageable pageable) {
        return eventRepository.findAllByOrderByDateDesc(pageable);
    }

    public Page<Event> findAllByClassificatorEqualsOrderByDateDesc(Pageable pageable, Classificator classificator) {
        return eventRepository.findAllByClassificatorEqualsOrderByDateDesc(pageable, classificator);
    }

}
