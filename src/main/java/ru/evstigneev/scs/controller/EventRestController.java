package ru.evstigneev.scs.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.Example;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.query.Param;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import ru.evstigneev.scs.entity.Event;
import ru.evstigneev.scs.enumeration.Classificator;
import ru.evstigneev.scs.service.EventService;

@Validated
@RestController
public class EventRestController {

    private final EventService eventService;

    @Autowired
    public EventRestController(EventService eventService) {
        this.eventService = eventService;
    }

    @GetMapping(value = "/events/{pageNumber}/{pageSize}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Find all events filtered by classificator if needed.")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 200, message = "Events have been found.")
    })
    public ResponseEntity<Page<Event>> findAll(@PathVariable("pageNumber") final int pageNumber, @PathVariable(
            "pageSize") final int pageSize, @Param("filter") final String filter) {
        final Page<Event> events;
        final Pageable eventsPageable = PageRequest.of(pageNumber, pageSize);

        try {
            if (filter == null || filter.isEmpty()) {
                events = eventService.findAllOrderByDateDesc(eventsPageable);
            } else {
                events = eventService.findAllByClassificatorEqualsOrderByDateDesc(eventsPageable,
                        Classificator.valueOf(filter));
            }
        } catch (EmptyResultDataAccessException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.ok(events);
    }

    @PostMapping(value = "/event", consumes =
            MediaType.APPLICATION_JSON_VALUE)
    @ApiOperation(value = "Create an event.")
    @ApiResponses(value = {
            @ApiResponse(code = 400, message = "Bad request."),
            @ApiResponse(code = 201, message = "The event has been created."),
            @ApiResponse(code = 409, message = "The event with this id already exists.")
    })
    public ResponseEntity<Event> create(@RequestBody @Valid final Event event) {
        final Event createdEvent;
        try {
            createdEvent = eventService.create(event);
        } catch (IllegalArgumentException ila) {
            return ResponseEntity.status(HttpStatus.CONFLICT).build();
        }
        return ResponseEntity.status(HttpStatus.CREATED).body(createdEvent);
    }

}
