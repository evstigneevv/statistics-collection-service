package ru.evstigneev.scs.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import ru.evstigneev.scs.enumeration.Classificator;

@Entity
@Table(name = "app_event")
public class Event {

    @Id
    private String id;
    @Column(name = "title", nullable = false)
    @NotBlank(message = "String is null or empty.")
    private String title;
    @Column(name = "value")
    @NotBlank(message = "String is null or empty.")
    private String value;
    @NotNull(message = "Date is null.")
    @Column(name = "date", nullable = false)
    private Date date;
    @Column(name = "classificator", nullable = false)
    @Enumerated(EnumType.STRING)
    private Classificator classificator;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Classificator getClassificator() {
        return classificator;
    }

    public void setClassificator(Classificator classificator) {
        this.classificator = classificator;
    }

}