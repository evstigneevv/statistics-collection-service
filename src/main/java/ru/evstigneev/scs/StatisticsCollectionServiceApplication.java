package ru.evstigneev.scs;

import java.util.Date;
import javax.annotation.PostConstruct;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ru.evstigneev.scs.controller.EventRestController;
import ru.evstigneev.scs.entity.Event;
import ru.evstigneev.scs.enumeration.Classificator;
import ru.evstigneev.scs.repository.EventRepository;
import ru.evstigneev.scs.service.EventService;

@SpringBootApplication
public class StatisticsCollectionServiceApplication {

    @Autowired
    EventRestController eventRestController;

    public static void main(String[] args) {
        SpringApplication.run(StatisticsCollectionServiceApplication.class, args);
    }

    @PostConstruct
    public void init() {
        Event eventOne = new Event();
        eventOne.setClassificator(Classificator.CLASSIFICATOR_ONE);
        eventOne.setDate(new Date());
        eventOne.setId("1");
        eventOne.setTitle("Title");
        eventOne.setValue("Value");
        Event eventTwo = new Event();
        eventTwo.setClassificator(Classificator.CLASSIFICATOR_TWO);
        eventTwo.setDate(new Date());
        eventTwo.setId("2");
        eventTwo.setTitle("Title");
        eventTwo.setValue("Value");
        eventRestController.create(eventOne);
        eventRestController.create(eventTwo);
    }

}
