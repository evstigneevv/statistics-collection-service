package ru.evstigneev.scs.repository;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import ru.evstigneev.scs.entity.Event;
import ru.evstigneev.scs.enumeration.Classificator;

@Repository
public interface EventRepository extends PagingAndSortingRepository<Event, String> {

    Page<Event> findAllByOrderByDateDesc(Pageable pageable);

    Page<Event> findAllByClassificatorEqualsOrderByDateDesc(Pageable pageable, Classificator classificator);

}
