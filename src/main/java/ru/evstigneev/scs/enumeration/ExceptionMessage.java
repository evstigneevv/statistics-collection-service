package ru.evstigneev.scs.enumeration;

public enum ExceptionMessage {

    ENTITY_ALREADY_EXISTS,
    ENTITY_DOES_NOT_EXIST

}
